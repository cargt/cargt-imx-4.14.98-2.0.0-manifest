# cargt-imx6sx-manifest

## Cargt Release Notes

This branch is based on 

* Poky: yocto-2.5 (sumo-19.0)
* FSL Community BSP: sumo
* QT: 5.10.1

## Cargt BSP Platform

To get the BSP you need to have `repo` installed and use it as:

Install the `repo` utility:

[source,console]
$: mkdir ~/bin
$: curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
$: chmod a+x ~/bin/repo

Download the BSP source:

[source,console]
$: PATH=${PATH}:~/bin
$: mkdir ~/my-yocto
$ cd ~/my-yocto
$ repo init -u git@bitbucket.org:cargt/cargt-imx-4.14.98-2.0.0-manifest.git -b imx-linux-sumo
$ repo sync -j4

At the end of the commands you have every metadata you need to start work with.

To start a simple image build:

[source,console]
$: MACHINE=<machine> DISTRO=<distro> source ./setup-environment <build directory>
$: bitbake fsl-image-qt5

You can use any directory to host your build.


